#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <program.h>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#define FRAME_DURATION 1.0 / 60.0

void on_resize( GLFWwindow * window, int width, int height )
{
	printf( "Resized : %dx%d\n", width, height );
}

int main( int argc, char * argv[] )
{
	if ( !glfwInit() ) return EXIT_FAILURE;
	
    glfwWindowHint( GLFW_CONTEXT_VERSION_MAJOR, 3 );
    glfwWindowHint( GLFW_CONTEXT_VERSION_MINOR, 2 );
    glfwWindowHint( GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE );
    glfwWindowHint( GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE );

	GLFWwindow * window = glfwCreateWindow( 800, 800, "", NULL, NULL );
	if ( !window ) return EXIT_FAILURE;
	
	glfwMakeContextCurrent( window );
    glfwSwapInterval( 0 );
	glfwSetWindowSizeCallback( window, &on_resize );

	if ( !gladLoadGL() )
	{
		fprintf( stderr, "Could not load OpenGL.\n" );
		
		glfwDestroyWindow( window );
		glfwTerminate();

		return EXIT_FAILURE;
	}

	printf( "OpenGL : %s\n", glGetString( GL_VERSION ) );
	printf( "GLSL   : %s\n", glGetString( GL_SHADING_LANGUAGE_VERSION ) );

	NaeProgram * prog = nae_program_new(
		"/Users/robinjungers/Desktop/basic.vert",
		"/Users/robinjungers/Desktop/basic.frag"
	);

	printf( "VS : %s\n", prog->vs_path );
	printf( "FS : %s\n", prog->fs_path );
	
	double fps = 60.0;

	while ( !glfwWindowShouldClose( window ) )
	{
		const double t = glfwGetTime();
		
		glClearColor( 0.9f, 0.9f, 0.9f, 1.f );
		glClear( GL_COLOR_BUFFER_BIT );
		glDisable( GL_DEPTH_TEST );
		
		glfwSwapBuffers( window );
		glfwPollEvents();

		double e = glfwGetTime() - t;
		if ( e < FRAME_DURATION )
		{
			struct timespec delay;
			delay.tv_sec = 0;
			delay.tv_nsec = ( long ) ( 1e9 * ( FRAME_DURATION - e ) );

			nanosleep( &delay, NULL );
		}

		e = glfwGetTime() - t;
		fps = 0.95 * fps + 0.05 / e;

		char title[6];
		snprintf( title, 6, "%dfps", ( int ) fps );
		glfwSetWindowTitle( window, title );
	}

	nae_program_free( prog );
	
	glfwDestroyWindow( window );
	glfwTerminate();

	return EXIT_SUCCESS;
}
